//
//  UIViewExt.swift
//  HeRe
//
//  Created by Scott Kornblatt on 11/1/17.
//  Copyright © 2017 Scott Kornblatt. All rights reserved.
//

import UIKit

extension UIView {
    func fadeTo(alphaValue: CGFloat, withDuration duration: TimeInterval) {
        UIView.animate(withDuration: duration) {
            self.alpha = alphaValue
        }
    }
}
