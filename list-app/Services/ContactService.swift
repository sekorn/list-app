//
//  ContactService.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/22/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import Foundation
import Contacts
import Firebase

class ContactService {
    static var instance = ContactService()
    
    internal var contacts: [User] = [User]()
    
    func GetContacts(completion: @escaping (_ contacts: [User]?) -> Void) {
        let store = CNContactStore()
        
        if CNContactStore.authorizationStatus(for: .contacts) == .notDetermined {
            store.requestAccess(for: .contacts, completionHandler: { (authorized, error) in
                if authorized {
                    self.retrieveContactsWith(store: store, handler: { (contacts) in
                        completion(contacts!)
                    })
                }
            })
        } else if CNContactStore.authorizationStatus(for: .contacts) == .authorized {
            self.retrieveContactsWith(store: store, handler: { (contacts) in
                completion(contacts!)
            })
        }
    }
    
    func retrieveContactsWith(store: CNContactStore, handler: @escaping(_ matching: [User]?) -> Void) {
        var results: [CNContact] = [CNContact]()
        var users: [User] = [User]()
        
        let keysToFetch = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactEmailAddressesKey, CNContactThumbnailImageDataKey, CNContactImageDataAvailableKey, CNContactPhoneNumbersKey] as [Any]
        
        var allContainers: [CNContainer] = []
        do {
            allContainers = try store.containers(matching: nil)
        } catch let error as NSError {
            print("error fetching containers: \(error)")
        }
        
        for container in allContainers {
            let predicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            do {
                let containerResults = try store.unifiedContacts(matching: predicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
            } catch let error as NSError {
                print("error: \(error)")
            }
        }
        
        DataService.instance.REF_USERS.getDocuments { (snapshot, error) in
            if let error = error {
                debugPrint("error fetching users: \(error)")
                return
            } else {
                if let snap = snapshot {
                    let contacts = User.parseData(snapshot: snap)
                    let newContacts = contacts.filter({ (user) -> Bool in
                        for contact in results {
                            for email in contact.emailAddresses {
                                let email1 = String.lowercased(email.value as String)
                                let email2 = String.lowercased(user.email)
                                if  email1() == email2() {
                                    return true
                                }
                            }
                        }
                        return false
                    })
                    users = newContacts
                }
            }
            handler(users)
        }
    }
}
