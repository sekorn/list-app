//
//  CommentService.swift
//  list-app
//
//  Created by Scott Kornblatt on 5/19/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import Foundation
import Firebase

class CommentService {
    static let instance = CommentService()
    
    func Add(comment: String, toItem item: Item, onList list: List, completion: (()->())?) {
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            // get item document
            let itemRef = DataService.instance.REF_LISTS
                .document(list.documentId!).collection(ITEMS)
                .document(item.documentId!)
            let itemDoc: DocumentSnapshot
            
            do {
                try itemDoc = transaction.getDocument(itemRef)
            } catch let error as NSError {
                debugPrint("Fetch error: \(error.localizedDescription)")
                return nil
            }
            
            // update comment count
            guard let oldNumComments = itemDoc.data()![NUM_COMMENTS] as? Int else { return 0 }
            transaction.updateData([
                NUM_COMMENTS : oldNumComments + 1,
                TIMESTAMP: Date()
                ], forDocument: itemDoc.reference)
            
            var comments: [String] = []
            if itemDoc.data()![COMMENTS] != nil {
                comments = itemDoc.data()![COMMENTS] as! [String]
            }
            
            comments.append(comment)
            item.comments = comments
            transaction.updateData([
                COMMENTS: item.comments], forDocument: itemDoc.reference)
            
            return oldNumComments + 1
        }) { (object, error) in
            if let error = error {
                debugPrint("error adding comments to item: \(error)")
                return
            }
        }
        
        if let completion = completion {
            completion()
        }
    }
    
    func Remove(comment: Comment, fromItem item: Item, onList list: List, completion: (()->())?) {
        DataService.instance.REF_LISTS
            .document(list.documentId!).collection(ITEMS)
            .document(item.documentId!).collection(COMMENTS)
            .document(comment.documentId!).delete { (error) in
                if let error = error {
                    debugPrint("error deleting comment: \(error)")
                }
                
                if let completion = completion {
                    completion()
                }
        }
    }
    
//    func Get(fromItem item: Item, onList list: List, completion: (()->())?) {
//        DataService.instance.REF_LISTS
//            .document(list.documentId!)
//            .collection(COMMENTS).getDocuments { (snapshot, error) in
//                if let error = error {
//                    debugPrint("error getting comments: \(error)")
//                }
//                
//                let comments = Comment.parseData(snapshot: snapshot)
//        }
//        
//        if let completion = completion {
//            completion()
//        }
//    }
}
