//
//  ItemService.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/20/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import Foundation
import Firebase

class ItemService {
    static let instance = ItemService()
    
    func Add(item: Item, toList list: List, completion: (()->())?) {
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            
            // get most recent list document
            let listRef = DataService.instance.REF_LISTS.document(list.documentId!)
            let listDoc: DocumentSnapshot
            
            do {
                try listDoc = transaction.getDocument(listRef)
            } catch let error as NSError {
                debugPrint("Fetch error: \(error.localizedDescription)")
                return nil
            }
            
            // update list numItems
            guard let oldNumItems = listDoc.data()![NUM_ITEMS] as? Int else { return nil }
            transaction.updateData([
                NUM_ITEMS : oldNumItems + 1,
                TIMESTAMP: Date()
                ], forDocument: listDoc.reference)
            
            // add item to list
            let newListItem = listRef.collection(ITEMS).document()
            item.setDocumentId(docId: newListItem.documentID)
            transaction.setData(item.toDictionary(), forDocument: newListItem)
            
            return oldNumItems + 1
        }) { (object, error) in
            if let error = error {
                debugPrint("error adding item to list: \(error)")
            }
        }
        
        if let completion = completion {
            completion()
        }
    }
    
    func Remove(item: Item, fromList list: List, completion: (()->())?) {
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            
            // get most recent list document
            let listRef = DataService.instance.REF_LISTS.document(list.documentId!)
            let listDoc: DocumentSnapshot
    
            do {
                try listDoc = transaction.getDocument(listRef)
            } catch let error as NSError {
                debugPrint("Fetch listDoc error: \(error.localizedDescription)")
                return nil
            }
            
            guard let oldNumItems = listDoc.data()![NUM_ITEMS] as? Int else { return nil }
            transaction.updateData([NUM_ITEMS: oldNumItems - 1], forDocument: listDoc.reference)
            
            let itemRef = listRef.collection(ITEMS).document(item.documentId!)
            let commentsRef = itemRef.collection(COMMENTS)
            commentsRef.getDocuments(completion: { (snapshot, error) in
                let comments = Comment.parseData(snapshot: snapshot)
                for comment in comments {
                    let commentRef = itemRef.collection(COMMENTS).document(comment.documentId!)
                    commentRef.delete()
                }
            })
            transaction.deleteDocument(itemRef)
            
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("error removing item from list: \(error)")
            }
        }
        
        if let completion = completion {
            completion()
        }
    }
    
    func Flag(item: Item, fromList listId: String, completion: (()->())?) {
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            
            let listRef = DataService.instance.REF_LISTS.document(listId)
            let itemRef = listRef.collection(ITEMS).document(item.documentId!)
            
            let listDocument: DocumentSnapshot
            let itemDocument: DocumentSnapshot
            
            do {
                try listDocument = transaction.getDocument(listRef)
            } catch let error as NSError {
                debugPrint("Fetch error: \(error.localizedDescription)")
                return nil
            }
            
            do {
                try itemDocument = transaction.getDocument(itemRef)
            } catch let error as NSError {
                debugPrint("Fetch error: \(error.localizedDescription)")
                return nil
            }
            
            transaction.updateData([TIMESTAMP : Date()], forDocument: listDocument.reference)
            
            guard let oldFlag = itemDocument.data()![FLAGGED] as? Bool else { return nil }
            transaction.updateData([FLAGGED : !oldFlag], forDocument: itemDocument.reference)
            
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("error running flag transaction: \(error)")
            }
        }
        
        if let completion = completion {
            completion()
        }
    }

    func UpdateQuantity(item: Item, to: Int, fromListId listId: String, completion: (()->())?) {
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            
            let listRef = DataService.instance.REF_LISTS.document(listId)
            let itemRef = listRef.collection(ITEMS).document(item.documentId!)
            
            let itemDocument: DocumentSnapshot
            
            do {
                try itemDocument = transaction.getDocument(itemRef)
            } catch let error as NSError {
                debugPrint("Fetch error: \(error.localizedDescription)")
                return nil
            }
            
            transaction.updateData([QUANTITY : to], forDocument: itemDocument.reference)
            
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("error running update quantity transaction: \(error)")
            }
        }
        
        if let completion = completion {
            completion()
        }
    }
}
