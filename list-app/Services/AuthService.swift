//
//  UserService.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/15/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import Foundation
import Firebase

class AuthService {
    static let instance = AuthService()
    
    func SignUp(username: String, email: String, password: String, callback: @escaping ()->()) {
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if let error = error {
                debugPrint("Error creating user: \(error.localizedDescription)")
            }
            
            let changeRequest = user?.createProfileChangeRequest()
            changeRequest?.displayName = username
            changeRequest?.commitChanges(completion: { (error) in
                if let error = error {
                    debugPrint(error.localizedDescription)
                }
            })
            
            guard let userid = user?.uid else { return }
            let newUser = User(username: username, email: email, listcount: 0, datecreated: Date(), documentId: userid)            
            
            UserService.instance.AddUser(user: newUser, completion: {
                callback()
            })
        }
    }
}
