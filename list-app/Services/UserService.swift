//
//  UpdateService.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/7/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import Foundation
import Firebase

class UserService {
    static let instance = UserService()
    
    func GetUser(uid: String, handler: @escaping(User) -> Void) {
        DataService.instance.REF_USERS.document(uid).getDocument { (document, error) in
            if let error = error {
                debugPrint("error retreiving user: \(error)")
            } else {
                if let doc = document {
                    let user = User(documentId: uid, doc: doc)
                    handler(user)
                }
            }
        }
    }
    
    func AddUser(user: User, completion: @escaping ()->()) {
        DataService.instance.REF_USERS.document(user.documentId!).setData(user.ToDictionary()) { (error) in
            if let error = error {
                debugPrint("error saving user: \(error)")
            } else {
                completion()
            }
        }
    }
    
    func SetCount(user: User) {
        DataService.instance.REF_USERS.document(user.documentId!).setData([LIST_COUNT : user.listcount], options: SetOptions.merge())
    }
}
