//
//  ListService.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/19/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import Foundation
import Firebase

class ListService {
    static let instance = ListService()
    
    func Add(list: List, toUser user: User, completion: (()->())?) {
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            
            // create a new list in Firestore
            let newList = DataService.instance.REF_LISTS.document()
            list.setDocumentId(docId: newList.documentID)
            transaction.setData(list.ToDictionary(), forDocument: newList)
            
            // add the user to the list-user collection
            let newListUser = newList.collection(USERS).document(user.documentId!)
            transaction.setData(user.ToDictionary(), forDocument: newListUser)
            
            // add the list to the user-list collection
            let newUserList = DataService.instance.REF_USERS
                .document(user.documentId!)
                .collection(LISTS)
                .document(newList.documentID)
            
            transaction.setData(list.ToDictionary(), forDocument: newUserList)
            
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("error running addList transaction: \(error)")
            }
        }
        
        if let completion = completion {
            completion()
        }
    }
    
    func Share(list: List, withUser user: User, completion: (()->())?) {
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            // get list document
            let listRef = DataService.instance.REF_LISTS.document(list.documentId!)
            
            // get user document
            let userRef = DataService.instance.REF_USERS.document(user.documentId!)
            
            // add user to list
            let listUserRef = listRef.collection(USERS)
            let listUserDoc = listUserRef.document(user.documentId!)
            transaction.setData(user.ToDictionary(), forDocument: listUserDoc)
            
            // add list to user
            let userListRef = userRef.collection(LISTS)
            let userListDoc = userListRef.document(list.documentId!)
            transaction.setData(list.ToDictionary(), forDocument: userListDoc)
            
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("error running shareList transaction: \(error)")
            }
        }
        
        if let completion = completion {
            completion()
        }
    }
    
    func Remove(list: List, fromUser user: User, completion: (()->())?) {
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            
            // get most recent list document
            let listRef = DataService.instance.REF_LISTS.document(list.documentId!)
            let listDocument: DocumentSnapshot
            
            do {
                try listDocument = transaction.getDocument(listRef)
            } catch let error as NSError {
                debugPrint("Fetch error: \(error.localizedDescription)")
                return nil
            }
            
            // remove user from list
            let listUserRef = listRef.collection(USERS).document(user.documentId!)
            transaction.deleteDocument(listUserRef)
            
            // update list numUsers
            guard let oldNumUsers = listDocument.data()![NUM_USERS] as? Int else { return nil }
            if oldNumUsers == 1 {
                // only one user in list, remove items, remove users, remove list, remove list from user
                
                // remove items
                let itemsRef = listRef.collection(ITEMS)
                itemsRef.getDocuments(completion: { (snapshot, error) in
                    if let error = error {
                        debugPrint("error occurred fetching items: \(error.localizedDescription)")
                    }
                    
                    let items = Item.parseData(snapshot: snapshot)
                    
                    for item in items {
                        let itemRef = listDocument.reference.collection(ITEMS).document(item.documentId!)
                        let commentsRef = itemRef.collection(COMMENTS)
                        
                        commentsRef.getDocuments(completion: { (snapshot, error) in
                            let comments = Comment.parseData(snapshot: snapshot)
                            
                            for comment in comments {
                                let commentRef = commentsRef.document(comment.documentId!)
                                commentRef.delete()
                            }
                        })
                        itemRef.delete()
                    }
                })
                
                // remove users from list
                let usersRef = listRef.collection(USERS)
                usersRef.getDocuments(completion: { (snapshot, error) in
                    if let error = error {
                        debugPrint("error occurred fetching users from list: \(error.localizedDescription)")
                    }
                    
                    let users = User.parseData(snapshot: snapshot)
                    
                    for user in users {
                        let userRef = listRef.collection(USERS).document(user.documentId!)
                        userRef.delete()
                    }
                })
                
                // remove list document
                transaction.deleteDocument(listDocument.reference)
                
                // remove list from user
                DataService.instance.REF_USERS.document(user.documentId!).collection(LISTS).document(listDocument.documentID).delete()
            } else {
                // more than one user in list, decrement the usercount
                transaction.updateData([NUM_USERS : oldNumUsers - 1], forDocument: listDocument.reference)
            }
            
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("error running removelist transaction: \(error)")
            }
        }
        
        if let completion = completion {
            completion()
        }
    }
    
    func GetFlagCount(list: List, completion: @escaping (Int?)->Void) {
        var count: Int = 0
        
        DataService.instance.REF_LISTS.document(list.documentId!).collection(ITEMS).getDocuments { (snapshot, error) in
            if let error = error {
                debugPrint("error fetching items: \(error.localizedDescription)")
            }
            
            let items = Item.parseData(snapshot: snapshot)
            for item in items {
                if item.flagged {
                    count += 1
                }
            }
            completion(count)
        }
    }
    
    func Sync(user: User, completion: (()->())?) {
        DataService.instance.REF_LISTS.getDocuments { (snapshot, error) in
            if let error = error {
                debugPrint("error fetching lists: \(error)")
            } else {
                let lists = List.parseData(snapshot: snapshot)
                
                var userLists: [List] = []
                
                for list in lists {
                    let listRef = DataService.instance.REF_LISTS.document(list.documentId!)
                    let usersInList = listRef.collection(USERS)
                    usersInList.getDocuments(completion: { (snapshot, error) in
                        if let error = error {
                            debugPrint("error getting users in list \(error)")
                        } else {
                            let users = User.parseData(snapshot: snapshot)
                            users.forEach({ (u) in
                                if u.documentId == user.documentId {
                                    if user.email == list.createdBy {
                                        list.setListType(type: "personal")
                                    } else {
                                        list.setListType(type: "shared")
                                    }
                                    userLists.append(list)
                                }
                            })
                        }
                        user.syncLists(userLists)
                    })
                }
            }
        }
        
        if let completion = completion {
            completion()
        }
    }
}
