//
//  DataService.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/7/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import Foundation
import Contacts
import Firebase

let DB_BASE = Firestore.firestore()
let settings = FirestoreSettings()

class DataService {
    static let instance = DataService()
    
    private var _REF_BASE = DB_BASE
    private var _REF_USERS = DB_BASE.collection(USERS)
    private var _REF_LISTS = DB_BASE.collection(LISTS)

    private init() {
        settings.isPersistenceEnabled = true
        _REF_BASE.settings = settings
    }
    
    var REF_USERS: CollectionReference {
        return _REF_USERS
    }
    
    var REF_LISTS: CollectionReference {
        return _REF_LISTS
    }
    
    var firestore: Firestore {
        return Firestore.firestore()
    }
    
    func isUserLoggedIn() -> Bool {
        return true
    }
}
