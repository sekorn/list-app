//
//  Alertable.swift
//  list-app
//
//  Created by Scott Kornblatt on 5/3/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import UIKit

protocol Alertable {}

extension Alertable where Self:UIViewController {
    func showAlert(_ title: String, _ msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }
    
    func showDeleteConfirm(_ title: String, _ msg: String, okHandler:(() -> ())?, cancelHandler: (() -> ())?) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            if let okHandler = okHandler {
                okHandler()
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            if let cancelHandler = cancelHandler {
                cancelHandler()
            }
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
}
