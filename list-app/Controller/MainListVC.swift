//
//  MainListVC.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/14/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import UIKit
import Firebase
import RevealingSplashView

enum ListType: String {
    case all = "all"
    case personal = "personal"
    case shared = "shared"
    case pantry = "pantry"
}

class MainListVC: UIViewController, Alertable {
    
    // outlets
    @IBOutlet weak var typeSegmented: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    // variables
    private(set) var lists = [List]()
    private(set) var listsCollectionRef: CollectionReference!
    private(set) var listsListener: ListenerRegistration!
    private(set) var selectedType = ListType.all.rawValue
    private(set) var handle: AuthStateDidChangeListenerHandle!
    private(set) var currentUser: User?
    
    let reavealingSplashView = RevealingSplashView(iconImage: UIImage(named: "Icon-Original")!, iconInitialSize: CGSize(width: 120, height: 120), backgroundColor: GREEN_COLOR)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 70
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.view.addSubview(reavealingSplashView)
        self.reavealingSplashView.animationType = .squeezeAndZoomOut
        self.reavealingSplashView.playSqueezeAnimation()
        
        let img = UIImage()
        self.navigationController?.navigationBar.shadowImage = img
        self.navigationController?.navigationBar.setBackgroundImage(img, for: .default)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            if let user = user {
                UserService.instance.GetUser(uid: user.uid, handler: { (user) in
                    self.currentUser = user
                    
                    // sync lists between lists and users
                    ListService.instance.Sync(user: user, completion: {
                        
                        // populate user lists
                        user.PopulateLists(completion: {
                            self.listsCollectionRef = DataService.instance.REF_USERS
                                .document(user.documentId!)
                                .collection(LISTS)
                            
                            self.setListener()
                            self.tableView.reloadData()
                        })
                    })
                })
            } else {
                self.currentUser = nil
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC")
                self.present(loginVC, animated: true, completion: nil)
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if listsListener != nil {
            listsListener.remove()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddList" {
            if let destinationVC = segue.destination as? AddListVC,
                let user = self.currentUser {
                
                destinationVC.currentUser = user
            }
        } else if segue.identifier == "toList" {
            if let destinationVC = segue.destination as? ListVC,
                let list = sender as? ListInfoCell,
                let user = self.currentUser {
                
                destinationVC.list = list.list
                destinationVC.currentUser = user
            }
        } else if segue.identifier == "toAddContact" {
            if let destinationVC = segue.destination as? AddContactVC,
                let user = self.currentUser,
                let list = sender as? List {
                destinationVC.list = list
                destinationVC.currentUser = user
            }
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier != "toAddList" { return true }
        
        if let user = currentUser {
            return user.listcount < 10
        } else {
            return false
        }
    }
    
    func setListener() {
        if selectedType == ListType.all.rawValue {
            listsListener = self.listsCollectionRef
                .order(by: TIMESTAMP, descending: true)
                .addSnapshotListener({ (snapshot, error) in
                    guard let snapshot = snapshot else {
                        debugPrint("error fetching lists: \(String(describing: error?.localizedDescription))")
                        return
                    }
                    
                    self.lists = List.parseData(snapshot: snapshot)
                    self.tableView.reloadData()
                })
        } else {
            listsListener = self.listsCollectionRef
                .whereField(TYPE, isEqualTo: selectedType)
                .order(by: TIMESTAMP, descending: true)
                .addSnapshotListener({ (snapshot, error) in
                    guard let snapshot = snapshot else {
                        debugPrint("error fetching lists: \(String(describing: error?.localizedDescription))")
                        return
                    }
                    
                    self.lists = List.parseData(snapshot: snapshot)
                    self.tableView.reloadData()
                })
        }
        
    }
    
    @IBAction func logoutButtonPressed(_ sender: Any) {
        do {
            try Auth.auth().signOut()
        } catch let error as NSError {
            showAlert("error", error.localizedDescription)
        }
    }
    @IBAction func listtypeChanged(_ sender: Any) {
        switch typeSegmented.selectedSegmentIndex {
        case 0:
            selectedType = ListType.all.rawValue
        case 1:
            selectedType = ListType.personal.rawValue
        case 2:
            selectedType = ListType.shared.rawValue
        case 3:
            selectedType = ListType.pantry.rawValue
        default:
            selectedType = ListType.all.rawValue
        }
        
        listsListener.remove()
        setListener()
    }
}

extension MainListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ListInfoCell", for: indexPath) as? ListInfoCell {
            cell.configureCell(list: lists[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = self.contextualDeleteAction(forRowAtIndexPath: indexPath)
        let share = self.contextualShareAction(forRowAtIndexPath: indexPath)
        let swipeActions = UISwipeActionsConfiguration(actions: [delete, share])
        return swipeActions
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit = self.contextualEditAction(forRowAtIndexPath: indexPath)
        let swipeActions = UISwipeActionsConfiguration(actions: [edit])
        return swipeActions
    }
    
    func contextualEditAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let list = self.lists[indexPath.row]
        let action = UIContextualAction(style: .normal, title: "") { (action, view, completion) in
            self.performSegue(withIdentifier: "toEditList", sender: list)
            completion(true)
        }
        action.image = UIImage(named: "edit")
        action.backgroundColor = BLUE_COLOR
        return action
    }
    
    func contextualDeleteAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let list = self.lists[indexPath.row]
        let action = UIContextualAction(style: .destructive, title: "") { (action, view, completion) in
            self.showDeleteConfirm("Remove '\(list.name!)' list", "are you sure you wish to remove this list?", okHandler: {
                ListService.instance.Remove(list: list, fromUser: self.currentUser!, completion: {
                    self.lists.remove(at: indexPath.row)
                    self.tableView.deleteRows(at: [indexPath], with: .fade)
                    self.currentUser?.setCount()
                    completion(true)
                })
            }, cancelHandler: {
                completion(false)
            })
        }
        action.image = UIImage(named: "delete")
        return action
    }
    
    func contextualShareAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: "") { (action, view, completion) in
            self.performSegue(withIdentifier: "toAddContact", sender: self.lists[indexPath.row])
            completion(true)
        }
        action.image = UIImage(named: "add-contact")
        action.backgroundColor = GREEN_COLOR
        return action
    }
}
