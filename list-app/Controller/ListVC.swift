//
//  ListVC.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/17/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import UIKit
import Firebase

class ListVC: UIViewController, Alertable {
    
    // outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var itemText: UITextField!
    @IBOutlet weak var addItemButton: UIButton!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var commentViewCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var saveCommentButton: UIButton!
    
    // variables
    private(set) var items = [Item]()
    private(set) var itemsCollectionRef: CollectionReference!
    private(set) var itemsListener: ListenerRegistration!
    private(set) var isTableEditing: Bool = false
    private(set) var currentItem: Item?
    private(set) var entryType: String?
    private(set) var state:String = ADD
    
    internal var currentUser: User!
    internal var list: List!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 70
        
        itemText.delegate = self
        itemText.autocorrectionType = .no
        itemText.returnKeyType = .done
        
        commentTextView.autocorrectionType = .no
        commentTextView.returnKeyType = .done
        
        commentTextView.delegate = self
        
        self.addKeyboardObservers()
        self.hideKeyboardWhenTappedAround()
        
        self.blurView.alpha = 0.0
        
        self.commentView.layer.cornerRadius = 5.0
        self.commentView.layer.masksToBounds = true
        
        self.saveCommentButton.layer.cornerRadius = 5.0
        self.saveCommentButton.layer.masksToBounds = true
        
        self.commentViewCenterConstraint.constant = self.view.frame.width
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissCommentView))
        self.blurView.addGestureRecognizer(tap)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = list.name
        self.list.PopulateUsers()
        self.list.PopulateItems()
        
        if let list = list {
            self.itemsCollectionRef = DataService.instance.REF_LISTS.document(list.documentId!).collection(ITEMS)
            self.setListener()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if itemsListener != nil {
            itemsListener.remove()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddContact" {
            if let destinationVC = segue.destination as? AddContactVC {
                if let list = sender as? List {
                    destinationVC.list = list
                    destinationVC.currentUser = currentUser
                }
            }
        }
    }
    
    func setListener() {
        itemsListener = self.itemsCollectionRef
            .order(by: FLAGGED, descending: false)
            .addSnapshotListener({ (snapshot, error) in
            guard let snapshot = snapshot else {
                debugPrint("error fetching items: \(String(describing: error?.localizedDescription))")
                return
            }
            
            self.items = Item.parseData(snapshot: snapshot)
            
            self.tableView.reloadData()
        })
    }
    
    func toggleState(){
        switch self.state {
        case ADD:
            self.state = CLOSE
        case CLOSE:
            self.state = ADD
        default:
            self.state = ADD
        }
    }
    
    func showCommentView() {
        self.commentViewCenterConstraint.constant = 0.0
        
        UIView.animate(withDuration: 0.5) {
            self.blurView.alpha = 1.0
            self.view.layoutIfNeeded()
        }
        
        self.commentTextView.becomeFirstResponder()
    }
    
    @objc func dismissCommentView() {
        self.commentViewCenterConstraint.constant = self.view.bounds.width
        
        UIView.animate(withDuration: 0.2) {
            self.blurView.alpha = 0.0
            self.view.layoutIfNeeded()
        }
        
        self.commentTextView.text.removeAll()
        self.commentTextView.textColor = UIColor.lightGray
        self.commentTextView.text = "add a comment"
    }
    
    @IBAction func addItemButtonPressed(_ sender: Any) {
        UIView.animate(withDuration: 0.5) {
            self.addItemButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi/4)
        }
        itemText.becomeFirstResponder()
    }
    
    @IBAction func addContactButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "toAddContact", sender: self.list)
    }
    
    @IBAction func saveCommentButtonPressed(_ sender: Any) {
        guard let user = self.currentUser else { return }
        guard let list = self.list else { return }
        guard let item = self.currentItem else { return }
        guard let text = commentTextView.text, !text.isEmpty else { return }
        
        self.shouldPresentLoadingView(true)
        
        let comment = Comment(text: text, createdBy: user.email, timestamp: Date())
        
        CommentService.instance.Add(comment: text, toItem: item, onList: list) {
            self.dismissCommentView()
            self.shouldPresentLoadingView(false)
        }
    }
}

extension ListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as? ItemCell {
            let item = items[indexPath.row]
            cell.configureCell(item: item, listId: list.documentId!, comments: item.comments)
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = self.contextualDeleteAction(forRowAtIndexPath: indexPath)
        let swipeActions = UISwipeActionsConfiguration(actions: [delete])
        return swipeActions
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let flag = self.contextualFlagAction(forRowAtIndexPath: indexPath)
        let swipeActions = UISwipeActionsConfiguration(actions: [flag])
        return swipeActions
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.items.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            ItemService.instance.Remove(item: self.items[indexPath.row],
                                        fromList: self.list,
                                        completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.currentItem = items[indexPath.row]
        if !self.currentItem!.flagged {
            self.showCommentView()
        }
    }
    
    func contextualFlagAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let item = self.items[indexPath.row]
        let action = UIContextualAction(style: .normal, title: "") { (action, view, completion) in
            ItemService.instance.Flag(item: item, fromList: self.list.documentId!, completion: {
                completion(true)
            })
        }
        action.backgroundColor = UIColor.blue
        action.image = UIImage(named: "flag")
        return action
    }
    
    func contextualDeleteAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let item = self.items[indexPath.row]
        let action = UIContextualAction(style: .destructive, title: "") { (action, view, completion) in
            
            self.showDeleteConfirm("Remove '\(item.name!)' from list?", "Are you sure you wish to remove this item from the list?", okHandler: {
                ItemService.instance.Remove(item: item, fromList: self.list) {
                    self.items.remove(at: indexPath.row)
                    self.tableView.deleteRows(at: [indexPath], with: .fade)
                    completion(true)
                }
            }, cancelHandler: {
                completion(false)
            })
            
        }
        action.image = UIImage(named: "delete")
        return action
    }
}

extension ListVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let user = self.currentUser else { return false }
        guard let list = self.list else { return false }
        guard let text = itemText.text, !text.isEmpty else { return false }
        
        let item = Item(name: text,
                        category: "default",
                        quantity: 1,
                        flagged: false,
                        requestedBy: user.email,
                        numComments: 0,
                        timestamp: Date(),
                        documentId: nil)
        
        ItemService.instance.Add(item: item, toList: list) {
            self.itemText.text?.removeAll()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.5) {
            self.addItemButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi/4)
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func hideKeyboard() {
        UIView.animate(withDuration: 0.5) {
            self.addItemButton.transform = CGAffineTransform(rotationAngle: 0.0)
        }
        self.dismissKeyboard()
        self.itemText.text?.removeAll()
    }
}

extension ListVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.commentTextView.text.removeAll()
        self.commentTextView.textColor = UIColor.black
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.dismissKeyboard()
    }
}
