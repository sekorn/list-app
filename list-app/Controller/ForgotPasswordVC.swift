//
//  ForgotPasswordVC.swift
//  list-app
//
//  Created by Scott Kornblatt on 5/4/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import UIKit
import Firebase

class ForgotPasswordVC: UIViewController {

    // outlets
    @IBOutlet weak var emailText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func resetPasswordButtonPressed(_ sender: Any) {
        guard let email = emailText.text else { return }
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            if let error = error {
                debugPrint("error occurred sending password reset: \(error.localizedDescription)")
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
