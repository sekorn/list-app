//
//  LoginVC.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/13/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import UIKit
import Firebase

class LoginVC: UIViewController, Alertable {
    
    // outlets
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loginButton.layer.cornerRadius = CORNER_RADIUS
        emailText.keyboardType = .emailAddress
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        guard let email = emailText.text,
            let password = passwordText.text else { return }
        
        self.shouldPresentLoadingView(true)
        
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let error = error {
                debugPrint("Error signing in: \(error)")
                self.showAlert("Error Signing In", "\(error)")
                self.shouldPresentLoadingView(false)
            } else {
                self.dismiss(animated: true, completion: nil)
                self.shouldPresentLoadingView(false)
            }
        }
    }
}
