//
//  AddListVC.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/14/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import UIKit
import Firebase

enum ListCategory: String {
    case grocery = "grocery"
    case home = "home"
    case takeout = "takeout"
    case other = "other"
}
class AddListVC: UIViewController {

    //outlets
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var categorySegmented: UISegmentedControl!
    @IBOutlet weak var createButton: UIButton!
    
    //variables
    private(set) var selectedCateogry = ListCategory.grocery.rawValue
    internal var currentUser: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createButton.layer.cornerRadius = CORNER_RADIUS
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func categoryChanged(_ sender: Any) {
        switch categorySegmented.selectedSegmentIndex {
        case 0:
            selectedCateogry = ListCategory.grocery.rawValue
        case 1:
            selectedCateogry = ListCategory.home.rawValue
        case 2:
            selectedCateogry = ListCategory.takeout.rawValue
        case 3:
            selectedCateogry = ListCategory.other.rawValue
        default:
            selectedCateogry = ListCategory.grocery.rawValue
        }
    }
    
    @IBAction func createButtonPressed(_ sender: Any) {
        guard let user = currentUser else { return }
        
        self.shouldPresentLoadingView(true)
        
        let list = List(name: nameText.text!,
                        type: "personal",
                        category: selectedCateogry,
                        numItems: 0,
                        createdBy: user.email!,
                        timestamp: Date(),
                        numUsers: 1,
                        documentId: nil)
        
        ListService.instance.Add(list: list, toUser: user) {
            self.navigationController?.popViewController(animated: true)
            self.shouldPresentLoadingView(false)
        }
    }
}
