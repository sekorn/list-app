//
//  AddContactVC.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/22/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import UIKit
import Firebase

class AddContactVC: UIViewController {
    
    // outlets
    @IBOutlet weak var contactsTableView: UITableView!
    @IBOutlet weak var addedContactsTableView: UITableView!
    
    // variables
    private(set) var addedUsers: [User] = [User]()
    private(set) var addedUsersSmall: [User]?
    private(set) var users: [User] = [User]()
    private(set) var listUserCollRef: CollectionReference!
    private(set) var userListener: ListenerRegistration!
    
    internal var list: List?
    internal var currentUser: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "add contacts"
        contactsTableView.delegate = self
        contactsTableView.dataSource = self
        
        addedContactsTableView.delegate = self
        addedContactsTableView.dataSource = self
        
//        ContactService.instance.GetContacts { (users) in
//            self.users = users!
//            DispatchQueue.main.async {
//                self.contactsTableView.reloadData()
//            }
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let _ = currentUser, let list = list {
            listUserCollRef = DataService.instance.REF_LISTS.document(list.documentId!).collection(USERS)
            setListener()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if userListener != nil {
            userListener.remove()
        }
    }
    
    func setListener() {
        userListener = listUserCollRef
            .addSnapshotListener({ (snapshot, error) in
                guard let snapshot = snapshot else {
                    debugPrint("error fetching users in list: \(String(describing: error?.localizedDescription))")
                    return
                }
                
                self.addedUsers = User.parseData(snapshot: snapshot)
                ContactService.instance.GetContacts(completion: { (users) in
                    if let users = users {
                        self.users = users.filter({ (user) -> Bool in
                            for au in self.addedUsers {
                                if au.email == user.email {
                                    return false
                                }
                            }
                            return true
                        })
                        
                        self.addedContactsTableView.reloadData()
                        self.contactsTableView.reloadData()
                    }
                })
            })
    }
}

extension AddContactVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == contactsTableView.self {
            return users.count
        } else if tableView == addedContactsTableView.self {
            return addedUsers.count - 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == contactsTableView.self {
            let contact = users[indexPath.row]
            addedUsersSmall = addedUsers.filter { (user) -> Bool in
                user.email == contact.email
            }
            if addedUsersSmall?.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath)
                cell.textLabel?.text = contact.email!
                return cell
            }
        } else if tableView == addedContactsTableView.self {
            let addedContact = addedUsers[indexPath.row]
            if addedContact.documentId != currentUser?.documentId {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddedContactCell", for: indexPath)
                cell.textLabel?.text = addedContact.email!
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let noDataLabel = UILabel(frame: self.view.bounds)
        noDataLabel.textColor = UIColor.white
        noDataLabel.textAlignment = .center
        
        var numberOfSections: Int = 0
        if tableView == contactsTableView.self {
            if users.count == 0 {
                noDataLabel.text = "No Contacts Found"
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
                numberOfSections = 1
            }
        } else if tableView == addedContactsTableView.self {
            if addedUsers.count == 0 {
                noDataLabel.text = "No Added Contacts Found"
                tableView.backgroundView = noDataLabel
            } else {
                numberOfSections = 1
                tableView.backgroundView = nil
            }
        }
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == contactsTableView.self {
            let user = users[indexPath.row]
            print(user.ToDictionary())
            ListService.instance.Share(list: list!, withUser: user, completion: nil)
        }
    }
}
