//
//  SignupVC.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/13/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import UIKit
import Firebase

class SignupVC: UIViewController {

    // outlets
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createButton.layer.cornerRadius = CORNER_RADIUS
        cancelButton.layer.cornerRadius = CORNER_RADIUS
        
        emailText.keyboardType = .emailAddress
        passwordText.keyboardType = .default
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func createButtonPressed(_ sender: Any) {
        guard let email = emailText.text,
            let password = passwordText.text,
            let username = usernameText.text else { return }
        
        self.shouldPresentLoadingView(true)
        
        AuthService.instance.SignUp(username: username, email: email, password: password) {
            self.dismiss(animated: true, completion: nil)
            self.shouldPresentLoadingView(false)
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
