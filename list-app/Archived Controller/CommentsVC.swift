//
//  CommentsVC.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/14/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import UIKit

class CommentsVC: UIViewController {
    // outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addCommentText: UITextField!
    
    // variables
    var thought: Thought!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addCommentButtonPressed(_ sender: Any) {
        
    }
}
