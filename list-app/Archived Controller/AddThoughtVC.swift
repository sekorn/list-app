//
//  AddThoughtVC.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/6/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import UIKit
import Firebase

class AddThoughtVC: UIViewController, UITextViewDelegate {

    //Outlets
    @IBOutlet private weak var categorySegment: UISegmentedControl!
    @IBOutlet private weak var usernameText: UITextField!
    @IBOutlet private weak var thoughtTextView: UITextView!
    @IBOutlet private weak var postButton: UIButton!
    
    //Variables
    private var selectedCategory = ThoughtCategory.funny.rawValue
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        postButton.layer.cornerRadius = 4.0
        thoughtTextView.layer.cornerRadius = 4.0
        thoughtTextView.text = "My random thought..."
        thoughtTextView.textColor = UIColor.lightGray
        thoughtTextView.delegate = self

        // Do any additional setup after loading the view.
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
        textView.textColor = UIColor.darkGray
    }
    
    @IBAction func categoryChanged(_ sender: Any) {
        switch categorySegment.selectedSegmentIndex {
        case 0:
            selectedCategory = ThoughtCategory.funny.rawValue
        case 1:
            selectedCategory = ThoughtCategory.serious.rawValue
        case 2:
            selectedCategory = ThoughtCategory.crazy.rawValue
        default:
            selectedCategory = ThoughtCategory.crazy.rawValue
        }
    }
    
    @IBAction func postButtonWasPressed(_ sender: Any) {
        Firestore.firestore().collection(THOUGHTS).addDocument(data: [
            CATEGORY : selectedCategory,
            NUM_COMMENTS: 0,
            NUM_LIKES: 0,
            THOUGHT_TXT: thoughtTextView.text,
            TIMESTAMP: FieldValue.serverTimestamp(),
            USERNAME: usernameText.text!
        ]) { (error) in
            if let error = error {
                debugPrint("error adding document: \(error)")
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
