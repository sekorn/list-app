//
//  CommentCell.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/14/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    
    // outlets
    @IBOutlet weak var commentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(comment: String) {
        self.commentLabel.text = comment
    }
}
