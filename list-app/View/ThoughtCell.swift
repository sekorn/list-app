//
//  ThoughtCell.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/11/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import UIKit
import Firebase

class ThoughtCell: UITableViewCell {

    // outlets
    @IBOutlet private weak var usernameLabel: UILabel!
    @IBOutlet private weak var timestampLabel: UILabel!
    @IBOutlet private weak var thoughtTxtLabel: UILabel!
    @IBOutlet private weak var likesImage: UIImageView!
    @IBOutlet private weak var likesNumLabel: UILabel!
    @IBOutlet weak var commentsImage: UIImageView!
    @IBOutlet weak var commentsNumLabel: UILabel!
    
    // variables
    private var thought: Thought!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(likeTapped))
        likesImage.addGestureRecognizer(tap)
        likesImage.isUserInteractionEnabled = true
    }
    
    @objc func likeTapped() {
        //Firestore.firestore().collection(THOUGHTS_REF).document(thought.documentId).setData([NUM_LIKES : thought.numLikes + 1], options: SetOptions.merge())
        
        Firestore.firestore().collection(THOUGHTS).document(thought.documentId).updateData([NUM_LIKES : thought.numLikes + 1])
    }
    
    func configureCell(thought: Thought) {
        self.thought = thought
        
        usernameLabel.text = thought.username
        thoughtTxtLabel.text = thought.thoughtTxt
        likesNumLabel.text = "\(thought.numLikes!)"
        commentsNumLabel.text = "\(thought.numComments!)"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, hh:mm"
        let timestamp = formatter.string(from: thought.timestamp)
        timestampLabel.text = timestamp
    }
}
