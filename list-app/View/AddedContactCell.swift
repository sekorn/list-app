//
//  AddedContactCell.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/22/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import UIKit

class AddedContactCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
