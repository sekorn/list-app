//
//  ListInfoCell.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/14/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import UIKit

class ListInfoCell: UITableViewCell {

    // outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var createdByLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    
    // variables
    internal var list: List!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(list: List) {
        self.list = list
        
        ListService.instance.GetFlagCount(list: list) { (count) in
            self.titleLabel.text = list.name
            
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM d, h:mm"
            self.timestampLabel.text = formatter.string(from: list.timestamp)
            self.createdByLabel.text = list.createdBy
            self.typeLabel.text = "\(list.category!.uppercased())"
            self.quantityLabel.text = "\(count!)/\(list.numItems!)"
        }
    }
}
