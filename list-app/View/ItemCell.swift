//
//  ItemCell.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/20/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import UIKit
import Firebase

class ItemCell: UITableViewCell {

    // outlets
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var itemCountLabel: UILabel!
    @IBOutlet weak var incrementButton: UIButton!
    @IBOutlet weak var decrementButton: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    
    // variables
    internal var item: Item!
    internal var listId: String!
    internal var quantity: Int!
    internal var comments: [String]?
    
    internal var commentRef: CollectionReference?
    internal var commentListener: ListenerRegistration?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(item: Item, listId: String, comments: [String]?) {
        self.item = item
        self.quantity = item.quantity
        self.listId = listId
        
        updateQuantityLabel(quantity: self.quantity)
        setDecrementButton()
        
        itemLabel.text = item.name
        itemLabel?.font = UIFont(name: "GillSans-Light", size: 20.0)
        
        if item.flagged {
            itemLabel?.textColor = UIColor.lightGray
            itemCountLabel?.textColor = UIColor.lightGray
            
            UIView.animate(withDuration: 0.2) {
                self.checkButton.setImage(UIImage(named: "Check2"), for: .normal)
            }
        } else {
            itemLabel?.textColor = UIColor.black
            itemCountLabel?.textColor = UIColor.black
            checkButton.setImage(UIImage(named: "Check"), for: .normal)
        }
        
        var commentValue = ""
        comments?.forEach({ (comment) in
            commentValue += "\(comment)\n"
        })
        self.commentLabel.text = commentValue
    }
    
    @IBAction func incrementButtonPressed(_ sender: Any) {
        if !self.item.flagged {
            self.quantity = self.quantity + 1
            updateQuantityLabel(quantity: self.quantity)
            setDecrementButton()
            ItemService.instance.UpdateQuantity(item: self.item, to: self.quantity, fromListId: self.listId, completion: nil)
        }
    }
    
    @IBAction func decrementButtonPressed(_ sender: Any) {
        if !self.item.flagged {
            if self.quantity > 0 {
                self.quantity = self.quantity - 1
                updateQuantityLabel(quantity: self.quantity)
                ItemService.instance.UpdateQuantity(item: self.item, to: self.quantity, fromListId: self.listId, completion: nil)
            }
            setDecrementButton()
        }
    }
    
    @IBAction func checkButtonPressed(_ sender: Any) {
        ItemService.instance.Flag(item: self.item, fromList: listId, completion: nil)
    }
    
    func setDecrementButton() {
        self.decrementButton.isEnabled = (self.quantity != 1)
    }
    
    func updateQuantityLabel(quantity: Int) {
        self.itemCountLabel.text = "\(quantity)"
    }
}
