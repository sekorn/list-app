//
//  Item.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/19/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import Foundation
import Firebase

class Item {
    private(set) var name: String!
    private(set) var category: String!
    private(set) var quantity: Int = 1
    private(set) var flagged: Bool = false
    private(set) var requestedBy: String!
    private(set) var numComments: Int = 0
    private(set) var timestamp: Date!
    private(set) var documentId: String?
    
    internal var comments: [String] = []
    
    init(name: String, category: String, quantity: Int, flagged: Bool, requestedBy: String, numComments: Int, timestamp: Date, documentId: String?) {
        self.name = name
        self.category = category
        self.quantity = quantity
        self.flagged = flagged
        self.requestedBy = requestedBy
        self.numComments = numComments
        self.timestamp = timestamp
        self.documentId = documentId
    }
    
    init(doc: QueryDocumentSnapshot) {
        let data = doc.data()
        self.name = data["name"] as? String
        self.category = data["category"] as? String
        self.quantity = data["quantity"] as? Int ?? 1
        self.flagged = data["flagged"] as? Bool ?? false
        self.requestedBy = data["requestedBy"] as? String
        self.comments = data["comments"] as? [String] ?? []
        self.numComments = data["numComments"] as? Int ?? 0
        self.timestamp = data["timestamp"] as? Date
        self.documentId = doc.documentID
    }
    
    func toDictionary() -> Dictionary<String, Any> {
        var dict = [String: Any]()
        dict["documentId"] = self.documentId
        dict["name"] = self.name
        dict["category"] = self.category
        dict["quantity"] = self.quantity
        dict["flagged"] = self.flagged
        dict["requestedBy"] = self.requestedBy
        dict["numComments"] = self.numComments
        dict["comments"] = self.comments
        dict["timestamp"] = self.timestamp
        return dict
    }
    
    func commentDict() -> Dictionary<Int, Any> {
        var count: Int = 0
        var dict = [Int: Any]()
        for comment in comments {
            dict[count] = comment
            count += 1
        }
        return dict
    }
    
    func setDocumentId(docId: String) {
        self.documentId = docId
    }
    
    class func parseData(snapshot: QuerySnapshot?) -> [Item] {
        var items = [Item]()
        
        guard let snap = snapshot else { return items }
        for doc in snap.documents {
            let newItem = Item(doc: doc)
            items.append(newItem)
        }
        
        return items
    }
}
