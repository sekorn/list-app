//
//  list.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/7/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import Foundation
import Firebase

class List: NSObject {
    private(set) var name: String!
    private(set) var type: String!
    private(set) var category: String!
    private(set) var items: [Item] = []
    private(set) var numItems: Int!
    private(set) var createdBy: String!
    private(set) var timestamp: Date!
    private(set) var users: [User] = []
    private(set) var numUsers: Int!
    private(set) var documentId: String?
    
    init(name: String, type:String, category: String, numItems: Int, createdBy: String, timestamp: Date, numUsers: Int, documentId: String?) {
        self.name = name
        self.type = type
        self.category = category
        self.numItems = numItems
        self.createdBy = createdBy
        self.timestamp = timestamp
        self.numUsers = numUsers
        self.documentId = documentId
    }
    
    init(doc: QueryDocumentSnapshot) {
        super.init()
        
        let data = doc.data()
        self.name = data["name"] as? String
        self.type = data["type"] as? String
        self.category = data["category"] as? String
        self.numItems = data["numItems"] as? Int ?? 0
        self.createdBy = data["createdBy"] as? String
        self.timestamp = data["timestamp"] as? Date
        self.numUsers = data["numUsers"] as? Int ?? 1
        self.documentId = doc.documentID
    }
    
    class func parseData(snapshot: QuerySnapshot?) -> [List] {
        var lists = [List]()
        
        guard let snap = snapshot else { return lists }
        for doc in snap.documents {
            let newList = List(doc: doc)
            lists.append(newList)
        }
        
        return lists
    }
    
    func setDocumentId(docId: String) {
        self.documentId = docId
    }
    
    func setListType(type: String) {
        self.type = type
    }
    
    func ToDictionary() -> Dictionary<String, Any> {
        var dict = [String: Any]()
        dict["documentId"] = self.documentId
        dict["name"] = self.name
        dict["type"] = self.type
        dict["category"] = self.category
        dict["numItems"] = self.numItems
        dict["createdBy"] = self.createdBy
        dict["timestamp"] = self.timestamp
        dict["numUsers"] = self.numUsers
        return dict
    }
    
    func PopulateUsers() {
        DataService.instance.REF_LISTS
            .document(self.documentId!)
            .collection(USERS)
            .getDocuments { (snapshot, error) in
            if let error = error {
                debugPrint("error getting list users: \(error)")
            } else {
                self.users = User.parseData(snapshot: snapshot)
            }
        }
    }
    
    func PopulateItems() {
        DataService.instance.REF_LISTS
            .document(self.documentId!)
            .collection(ITEMS)
            .getDocuments { (snapshot, error) in
            if let error = error {
                debugPrint("error getting list items: \(error)")
            } else {
                self.items = Item.parseData(snapshot: snapshot)
            }
        }
    }
}
