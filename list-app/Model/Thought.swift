//
//  Thought.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/11/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import Foundation
import Firebase

class Thought {
    
    private(set) var username: String!
    private(set) var timestamp: Date!
    private(set) var thoughtTxt: String!
    private(set) var numLikes: Int!
    private(set) var numComments: Int!
    private(set) var documentId: String!
    
    init(username: String, timestamp: Date, thoughtTxt: String, numLikes: Int, numComments: Int, documentId: String) {
        self.username = username
        self.timestamp = timestamp
        self.thoughtTxt = thoughtTxt
        self.numLikes = numLikes
        self.numComments = numComments
        self.documentId = documentId
    }
    
    init(doc: QueryDocumentSnapshot) {
        let data = doc.data()
        self.username = data[USERNAME] as? String ?? "Anonymous"
        self.timestamp = data[TIMESTAMP] as? Date ?? Date()
        self.thoughtTxt = data[THOUGHT_TXT] as? String ?? ""
        self.numLikes = data[NUM_LIKES] as? Int ?? 0
        self.numComments = data[NUM_COMMENTS] as? Int ?? 0
        self.documentId = doc.documentID
    }
    
    class func parseData(snapshot: QuerySnapshot?) -> [Thought] {
        var thoughts = [Thought]()
        
        guard let snap = snapshot else { return thoughts }
        for doc in snap.documents {
            let newThought = Thought(doc: doc)
            thoughts.append(newThought)
        }
        
        return thoughts
    }
}
