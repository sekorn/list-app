//
//  user.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/8/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import Foundation
import Firebase

class User: NSObject {
    private(set) var username: String!
    private(set) var email: String!
    private(set) var listcount: Int!
    private(set) var lists: [List] = []
    private(set) var datecreated: Date!
    private(set) var documentId: String?
    
    init(username: String, email: String, listcount: Int, datecreated: Date, documentId: String?) {
        self.username = username
        self.email = email
        self.listcount = listcount
        self.datecreated = datecreated
        self.documentId = documentId
    }
    
    init(documentId: String, doc: DocumentSnapshot) {
        super.init()
        
        let data = doc.data()!
        self.username = data["username"] as? String
        self.email = data["email"] as? String
        self.listcount = data["listcount"] as? Int ?? 0
        self.datecreated = data["datecreated"] as? Date
        self.documentId = documentId
    }
    
    init(doc: QueryDocumentSnapshot) {
        let data = doc.data()
        
        self.username = data["username"] as? String
        self.email = data["email"] as? String
        self.listcount = data["listcount"] as? Int ?? 0
        self.datecreated = data["datecreated"] as? Date
        self.documentId = doc.documentID
    }
    
    func PopulateLists(completion: (()->())?) {
        DataService.instance.REF_USERS
            .document(self.documentId!)
            .collection(LISTS)
            .getDocuments(completion: { (snapshot, error) in
            if let error = error {
                debugPrint("error getting user lists: \(error)")
            } else {
                self.lists = List.parseData(snapshot: snapshot)
                self.setCount()
                
                if let completion = completion {
                    completion()
                }
            }
        })
    }
    
    func addList(list: List) {
        self.lists.append(list)
    }
    
    func setCount() {
        listcount = lists.count
        UserService.instance.SetCount(user: self)
    }
    
    func syncLists(_ lists: [List]) {
        self.lists = lists
        
        let userRef = DataService.instance.REF_USERS.document(self.documentId!)
        let userListsRef = userRef.collection(LISTS)
        
        self.lists.forEach { (list) in
            
//            // get reference to list object on user and delete it
//            let userListDocRef = listsRef.document(list.documentId!)
//            userListDocRef.delete()
//
//            // add new list
//            let newList = listsRef.document(list.documentId!)
//            newList.setData(list.ToDictionary())
            
            // get reference to list object on user
            userListsRef.document(list.documentId!).getDocument(completion: { (snapshot, error) in
                if let error = error {
                    debugPrint("error getting document \(error)")
                    let newList = userListsRef.document(list.documentId!)
                    newList.setData(list.ToDictionary())
                } else {
                    userListsRef.document(list.documentId!).updateData(list.ToDictionary())
                }
            })
        }
    }
    
    func ToDictionary() -> Dictionary<String, Any> {
        var dict = Dictionary<String, Any>()
        dict["username"] = self.username
        dict["email"] = self.email
        dict["listcount"] = self.listcount
        dict["datecreated"] = self.datecreated
        return dict
    }
    
    func compare() -> Dictionary<String, Any> {
        var dict = Dictionary<String, Any>()
        dict["username"] = self.username
        dict["email"] = self.email
        return dict
    }
    
    class func parseData(snapshot: QuerySnapshot?) -> [User] {
        var users = [User]()
        
        guard let snap = snapshot else { return users }
        for doc in snap.documents {
            let newUser = User(doc: doc)
            users.append(newUser)
        }
        
        return users
    }
}
