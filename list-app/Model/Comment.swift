//
//  comment.swift
//  list-app
//
//  Created by Scott Kornblatt on 5/19/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import Foundation
import Firebase

class Comment: NSObject {
    private(set) var text: String!
    private(set) var createdBy: String!
    private(set) var timestamp: Date!
    private(set) var documentId: String?
    
    init(text: String, createdBy: String, timestamp: Date) {
        self.text = text
        self.createdBy = createdBy
        self.timestamp = timestamp
    }
    
    init(doc: QueryDocumentSnapshot) {
        super.init()
        
        let data = doc.data()
        self.text = data["text"] as? String
        self.createdBy = data["createdBy"] as? String
        self.timestamp = data["timestamp"] as? Date
        self.documentId = data["documentId"] as? String
    }
    
    func toDictionary() -> Dictionary<String, Any> {
        var dict = [String: Any]()
        dict["documentId"] = self.documentId
        dict["text"] = self.text
        dict["createdBy"] = self.createdBy
        dict["timestamp"] = self.timestamp
        return dict
    }
    
    func setDocumentId(docId: String) {
        self.documentId = docId
    }
    
    class func parseData(snapshot: QuerySnapshot?) -> [Comment] {
        var comments = [Comment]()
        
        guard let snap = snapshot else { return comments }
        for doc in snap.documents {
            let newComment = Comment(doc: doc)
            comments.append(newComment)
        }
        
        return comments
    }
}
