//
//  Constants.swift
//  list-app
//
//  Created by Scott Kornblatt on 4/9/18.
//  Copyright © 2018 Scott Kornblatt. All rights reserved.
//

import Foundation
import UIKit

let GREEN_COLOR = UIColor(displayP3Red: 132/255, green: 200/255, blue: 65/255, alpha: 1.0)
let BLUE_COLOR = UIColor(displayP3Red: 74/255, green: 144/255, blue: 226/255, alpha: 1.0)

let CORNER_RADIUS: CGFloat = 2.0

let THOUGHTS = "thoughts"
let USERS = "users"
let LISTS = "lists"
let ITEMS = "items"
let COMMENTS = "comments"

let ITEM = "item"
let COMMENT = "comment"

let ADD = "add"
let CLOSE = "close"

let TYPE = "type"
let CATEGORY = "category"
let NUM_COMMENTS = "numComments"
let NUM_LIKES = "numLikes"
let NUM_ITEMS = "numItems"
let NUM_USERS = "numUsers"
let THOUGHT_TXT = "thoughtText"
let TIMESTAMP = "timestamp"
let USERNAME = "username"
let DATE_CREATED = "datecreated"
let FLAGGED = "flagged"
let LIST_COUNT = "listcount"
let QUANTITY = "quantity"
